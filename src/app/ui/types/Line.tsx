export default interface Line {
  id: Number;
  transcript_id: Number;
  speaker_id: Number;
}

