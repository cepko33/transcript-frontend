export interface NewSpeaker {
  name: String;
  color: String;
}

export default interface Speaker {
  id: Number;
  name: String;
  color: String;
}

