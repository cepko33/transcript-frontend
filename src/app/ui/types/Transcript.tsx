export default interface Transcript {
  id: number;
  name: string;
  subtitle: string;
  description: string;
  source_url: string;
  header_photo_url: string;
  episode_number: number;
}
