export default interface Word {
  id: Number;
  line_id: Number;
  content: String;
  start_time: Number;
  end_time: Number;

}

