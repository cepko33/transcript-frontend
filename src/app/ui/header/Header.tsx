'use client'
import * as React from "react"
import { Dropdown, Select } from 'react-daisyui'
const { Option } = Select
import {setTheme} from '@/app/actions'

async function HeaderElement({theme}: any) {
    return (
        <div className="navbar bg-base-300" data-theme={theme}>
            <div className="navbar-start">
            </div>
            <div className="navbar-center">
                <a className="btn btn-ghost text-xl" href="/">Unsettled.FM</a>
            </div>
            <div className="navbar-end">
                <Select size="md" value={theme} onChange={(event) => { setTheme(event.target.value) }}>
                    <Option value="default">Default</Option>
                    <Option value="light">Light</Option>
                    <Option value="dark">Dark</Option>
                    <Option value="retro">Retro</Option>
                    <Option value="night">Night</Option>
                    <Option value="cupcake">Cupcake</Option>
                    <Option value="cmyk">CMYK</Option>
                </Select>
            </div>
        </div>
    )
}

export default HeaderElement