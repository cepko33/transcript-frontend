import Word from '@/app/ui/types/Word'
import * as React from "react"
import { FaCheck, FaBan } from "react-icons/fa"
import { Popover } from 'flowbite-react'

export const postWord = async (word: Word) => {
  const res = await fetch(`https://${process.env.API_HOSTNAME}/api/word`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(word)
    })

  if (!res.ok) {
    throw new Error("Failed to update word")
  }

  return res.json()
}

const WordElement = ({ wordInput, playerRef, time, scrollToMiddle, speaker }: any) => {
  const [word, setWord] = React.useState(wordInput)
  const [isEditing, setIsEditing] = React.useState(false)
  const timer = React.useRef<any>()
  const scrollRef = React.useRef<any>()

  const [tempWord, setTempWord] = React.useState(word.content)

  const onClickHandler = (event: React.MouseEvent<HTMLDivElement>) => {
    clearTimeout(timer.current);

    if (event.detail === 1) {
      timer.current = setTimeout(() => { playerRef.current.seekTo(word.start_time, 'seconds') }, 200)
    } else if (event.detail === 2 && process.env.NODE_ENV === 'development') {
      setIsEditing(true)
    }
  }

  const persistWord = async () => {
    const newWord = await postWord({ ...word, content: tempWord })
    setWord({ ...word, content: tempWord })
    setIsEditing(false)
  }

  const cancelEdit = async () => {
    setTempWord(word.content)
    setIsEditing(false)
  }

  const plain = (time = 0) => (
    <span
      key={word.start_time}
      className={`
       font-serif 
        text-2xl
        rounded
        ${time >= word.end_time ? 'bg-base-200' : ''}
        ${time + 0.5 >= word.end_time ? 'bg-base-500' : ''}
        ${time >= word.start_time && time < word.end_time ? 'bg-warning text-warning-content' : ''}
      `}
      style={{ opacity: 1 }}
      onClick={onClickHandler}
      ref={scrollRef}
    >
      {`${["'d", "'s", ",", ".", ":", "?", "'t", "'ve"].includes(word.content) ? '' : ' '}` + word.content}
    </span>
  )

  const editable = () => (
    <Popover
      placement="top"
      aria-labelledby="word-edit-popover"
      content={
        <span>
          <button
            onClick={() => persistWord()}
            title="Save"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold p-1 my-1 mx-1 rounded"
          >
            <FaCheck />
          </button>
          <button
            title="Cancel"
            onClick={() => cancelEdit()}
            className="bg-red-500 hover:bg-red-700 text-white font-bold p-1 my-1 mx-1 rounded"
          >
            <FaBan />
          </button>
        </span>
      }
    >
      <span
        className="input active:input-primary focus:input-secondary hover:input-accent  mx-1 text-xl px-1"
        contentEditable
        role="textbox"
        onBlur={e => setTempWord(e.currentTarget.textContent)}
      >
        {tempWord}
      </span>
    </Popover>
  )

  if (scrollRef.current && scrollToMiddle && !isEditing && time >= word.start_time && time <= word.end_time) {
    scrollRef.current!.scrollIntoView({ behavior: "auto", block: "center" })
  }

  return isEditing ? editable() : plain(time)
}

export default WordElement
