'use client'

import * as React from "react"
import ReactPlayer from 'react-player'
import { FaPlay, FaStop, FaVolumeUp, FaVolumeMute, FaCheck, FaBan } from "react-icons/fa";
import Slider from 'rc-slider'
import 'rc-slider/assets/index.css'

import DOMPurify from 'isomorphic-dompurify'

import { Dropdown, Select } from 'react-daisyui'
const { Option } = Select

/*
import { createClient } from '@supabase/supabase-js'
const supabase = createClient("https://supabase.localhost", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyAgCiAgICAicm9sZSI6ICJhbm9uIiwKICAgICJpc3MiOiAic3VwYWJhc2UtZGVtbyIsCiAgICAiaWF0IjogMTY0MTc2OTIwMCwKICAgICJleHAiOiAxNzk5NTM1NjAwCn0.dc_X5iR_VP_qT0zsiyj_I_OZ2T9FtRU2BBNWN8Bu4GE")
*/


import Word from "./Word"
import Line from "./Line"

const TranscriptElement = ({ inputJSON }: any) => {
  const [time, setTime] = React.useState(0.0)
  const [timePerc, setTimePerc] = React.useState(0.0)

  const [transcript, setTranscript] = React.useState(inputJSON[0])
  const [lines, setLines] = React.useState(inputJSON[1].map((o: any) => o[0]))
  const [words, setWords] = React.useState(Object.fromEntries(inputJSON[1].map((lines: any) => [lines[0].id, lines[1]])))
  const [speakers, setSpeakers] = React.useState(inputJSON[2])

  const [scrollToMiddle, setScrollToMiddle] = React.useState(false)
  const [playing, setPlaying] = React.useState(false)
  const [volume, setVolume] = React.useState(0.8)
  const [muted, setMuted] = React.useState(false)


  const [isClient, setIsClient] = React.useState(false)
 
  React.useEffect(() => {
    setIsClient(true)
  }, [])

  const playerRef = React.useRef<ReactPlayer>(null)
  const script = lines.map((line: any, i: number, array: any) => (
    <Line
      key={`line-${line.id}`}
      lineInput={line}
      setLines={setLines}
      lines={lines}
      words={words[line.id] || []}
      speakers={speakers}
      setSpeakers={setSpeakers}
      playerRef={playerRef}
      time={time}
      previousLine={i === 0 ? false : array[i - 1]}
      scrollToMiddle={scrollToMiddle}
    />
  ))

  /*
  const changes = supabase.channel('line-changes')
   .on(
     'postgres_changes',
     {
       event: 'INSERT',
       schema: 'public',
       table: 'lines',
       filter: `transcript_id=eq.${transcript.id}`
     },
     payload => setLines([...lines, payload.new])
   ).subscribe()
 */

  const [theme, setTheme] = React.useState('default')

  const description = DOMPurify.sanitize(transcript.description, { USE_PROFILES: { html: true } })

  return (
    <div className="flex flex-col overflow-hidden" data-theme={theme} style={{ height: 'calc(100dvh - 4rem)' }}>
      <main className="overflow-y-scroll min-h-[90%]">
        <div className="px-6 sm:px-24 container mx-auto mt-8">
          <div className="mb-8 flex flex-row">
            <div
              className={`h-64 md:h-[36rem] sm:h-96 flex flex-col w-full bg-cover lg:bg-auto sm:bg-[top_-3rem_left5rem] md:bg-[top_4rem_left_4rem] lg:bg-[top_4rem_right_-3rem] xl:bg-[top_4rem_right_-3rem] bg-[top_3rem_left_2rem] bg-no-repeat bg-[image:var(--image-url)] `}
              style={{
                '--image-url': `url('${transcript.header_photo_url}')`,
                backgroundSize: 'auto 100%'
              } as any}
            >
              <div className="sm:w-2/3 xl:w-1/2">
                <Dropdown
                  vertical={"bottom"}
                >
                  <Dropdown.Toggle button={false} >
                    <h1
                      className="pu-1 pl-1 mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-8xl text-primary"
                    //style={{textShadow: '2px 2px 3px white'}}
                    >
                      {transcript.title}
                    </h1>
                  </Dropdown.Toggle>
                  <Dropdown.Menu
                    className="card  drop-shadow-xl p-2 "
                    style={{ zIndex: 5 }}
                  >
                    <div dangerouslySetInnerHTML={{ __html: description }} />
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
          </div>
          {script}
        </div>
      </main>
      <footer className="w-full text-center border-t border-base-200 bg-base-300 h-[78rem] sm:h-[32rem]">
        {isClient && (<>

        <ReactPlayer
          url={transcript.source_url}
          controls={false}
          width={0}
          height={0}
          onProgress={(({ playedSeconds, played }) => { setTime(playedSeconds); setTimePerc(played) })}
          playing={playing}
          volume={volume}
          muted={muted}
          progressInterval={100}
          ref={playerRef}
        />
        <div className="container mx-auto p-4">
          <div className="flex gap-2 sm:gap-8">
            <button onClick={() => setPlaying(!playing)} className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded flex-none mr-2">
              {playing ? <FaStop /> : <FaPlay />}
            </button>
            <Slider
              className="mt-2 grow"
              styles={{
              }}
              min={0}
              max={1}
              step={0.001}
              value={timePerc}
              onChange={(perc) => {
                if (Array.isArray(perc)) {
                } else {
                  setTimePerc(perc);
                  playerRef.current?.seekTo(perc, 'fraction')
                }
              }
              }
            />
            <button className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded flex-none " onClick={() => { return setMuted(!muted) }}
            >
              {muted ? <FaVolumeMute /> : <FaVolumeUp />}
            </button>
            <button onClick={() => setScrollToMiddle(!scrollToMiddle)} className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded text-xs flex-none ">
              {scrollToMiddle ? 'Static' : 'Follow'}
            </button>
          </div>
        </div>
      </>)}
      </footer>
    </div>
  )
}

export default TranscriptElement;
