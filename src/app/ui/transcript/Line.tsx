import * as React from "react"
import Word, { postWord } from "./Word"
import Speaker, { NewSpeaker } from '@/app/ui/types/Speaker'
import Line from '@/app/ui/types/Line'
import { FaCheck, FaBan, FaAngleUp, FaAngleDown } from "react-icons/fa";

import { Collapse, Input, Card } from 'react-daisyui'

import CreatableSelect from 'react-select/creatable';

const postSpeakerAssign = async (speaker: NewSpeaker) => {
  const res = await fetch(`https://${process.env.API_HOSTNAME}/api/speaker/new`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(speaker)
    })

  if (!res.ok) {
    throw new Error("Failed to create speaker")
  }

  return res.json()
}

const postSpeakerUpdate = async (speaker: Speaker) => {
  const res = await fetch(`https://${process.env.API_HOSTNAME}/api/speaker`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(speaker)
    })

  if (!res.ok) {
    throw new Error("Failed to create speaker")
  }

  return res.json()
}


const postSpeakerLine = async (line: Line) => {
  const res = await fetch(`https://${process.env.API_HOSTNAME}/api/line`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(line)
    })

  if (!res.ok) {
    throw new Error("Failed to create line")
  }

  return res.json()
}

const LineElement = ({ lineInput, words, lines, setLines, setSpeakers, speakers, playerRef, time, scrollToMiddle }: any) => {
  const [line, setLine] = React.useState(lineInput)
  const [speaker, setSpeaker] = React.useState(speakers.find((s: any) => s.id === line.speaker_id) || { name: '', color: '#fff' })
  const [wordList, setWordList] = React.useState(words || [])

  const [isEditing, setIsEditing] = React.useState(false)
  const [tempSpeaker, setTempSpeaker] = React.useState(speaker || { name: '', color: '#fff' })
  const timer = React.useRef<any>()

  /*
  const changes = supabase.channel('word-changes')
    .on(
      'postgres_changes',
      {
        event: 'INSERT',
        schema: 'public',
        table: 'words',
        filter: `line_id=eq.${line.id}`
      },
      payload => setWordList([...wordList, payload.new])
    ).subscribe()
  */


  const wordEls = wordList.map((word: any) => (<Word key={`word-${word.id}`} wordInput={word} playerRef={playerRef} time={time} scrollToMiddle={scrollToMiddle} />))

  const moveOneBack = async () => {
    const newWord = { ...wordList.at(0), line_id: line.id - 1 }

    await postWord(newWord)

    setLines(
      lines.map((l: any) => {
        if (l[0].id == line.id) {
          return [l[0], l[1].slice(1)]
        } else if (l[0].id == line.id - 1) {
          return [l[0], [...l[1], newWord]]
        } else {
          return l
        }
      })
    )
  }

  const moveOneForward = async () => {
    const newWord = { ...wordList.at(-1), line_id: line.id + 1 }

    await postWord(newWord)

    setLines(
      lines.map((l: any) => {
        if (l[0].id == line.id) {
          return [l[0], l[1].slice(0, -1)]
        } else if (l[0].id == line.id + 1) {
          return [l[0], [newWord, ...l[1]]]
        }
        return l
      })
    )
  }

  const color = [
    "primary", 
    "secondary",
    "info",
    "success",
    "warning",
    "error",
  ]

  const getColor = color[speaker.id % color.length]


  const onClickHandler = (event: React.MouseEvent<HTMLDivElement>) => {
    clearTimeout(timer.current);

    if (event.detail === 1) {
      timer.current = setTimeout(() => { }, 200)
    } else if (event.detail === 2 && process.env.NODE_ENV === 'development') {
      setIsEditing(true)
    }
  }


  const plain = (time = 0, scrollToMiddle = false) => (
    <Card
      bordered={false}
      className="card-compact my-1 shadow-sm "
    >
      <Card.Body>
        <Collapse>
          <Collapse.Title className="p-0">
            <div
              className="py-2 hover:bg-base-200 p-1"
            >
              <div className="flex flex-row py-2">
                <div onClick={onClickHandler} className={`shadow-md border-${getColor} bg-${getColor} border-2 px-1 font-semibold text-base-100`}>
                  {speaker.name || (process.env.NODE_ENV === 'development' && '--')}
                </div>
                {(() => {
                  let date = new Date(0)
                  date.setSeconds(wordList[0].start_time)
                  return date.toISOString().substring(11, 19)
                })()}
              </div>
              <article className="text-wrap">
                <p>{wordEls}</p>
              </article>
            </div>
          </Collapse.Title>
        </Collapse>
      </Card.Body>
    </Card>
  )

  const createSpeaker = async (inputValue: string) => {
    const newSpeaker = await postSpeakerAssign({ name: inputValue, color: "#725f23" })
    const updateLine = await postSpeakerLine({ id: line.id, speaker_id: newSpeaker.id, transcript_id: line.transcript_id })
    setLine(updateLine)
    setSpeakers([...speakers, newSpeaker])
    setSpeaker(newSpeaker)
    setIsEditing(false)
  }

  const persistSpeaker = async () => {
    const updateLine = await postSpeakerLine({ id: line.id, speaker_id: speaker.id, transcript_id: line.transcript_id })
    const updateSpeaker = await postSpeakerUpdate(speaker)
    setLine(updateLine)
    setSpeaker(updateSpeaker)
    setIsEditing(false)
  }


  const editable = (time = 0) => (
    <div className="py-2 hover:bg-base-200 p-1">
      <span>
        <div className="flex flex-row">
          <CreatableSelect
            value={speaker.id}
            onCreateOption={createSpeaker}
            onChange={(s) => { setSpeaker(speakers.find((x: any) => x.id === s.value)) }}
            options={speakers.map((s: any) => {
              return { label: s.name, value: s.id };
            })}
            className="w-96"
          />
          <Input
            value={speaker.color}
            onChange={e => { setSpeaker({ ...speaker, color: e.target.value }) }}
          />
          <button
            onClick={() => setIsEditing(false)}
            className="bg-red-500 hover:bg-red-700 text-white font-bold p-1 my-1 mx-1 rounded"
          >
            <FaBan />
          </button>
          <button
            type="button"
            onClick={() => { persistSpeaker() }}
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold p-1 my-1 mx-1 rounded"
          >
            <FaCheck />
          </button>
          <button
            type="button"
            onClick={moveOneBack}
          >
            <FaAngleUp />
          </button>
          <button
            type="button"
            onClick={moveOneForward}
          >
            <FaAngleDown />
          </button>


        </div>
        <p className="text-xs">enter or choose a speaker</p>
        <div onClick={onClickHandler} className={`text-[${speaker.color}]`}>
          {speaker.name || (process.env.NODE_ENV === 'development' && '--')}
        </div>
      </span>
      <span>
      </span>
      <p>
        {wordEls}
      </p>
    </div>
  )

  return wordList.length > 0 ? (isEditing ? editable(time) : plain(time, scrollToMiddle)) : false
}


export default LineElement
