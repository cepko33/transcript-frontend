import Transcript from '@/app/ui/types/Transcript'
import { FaUniversity, FaGitlab } from "react-icons/fa";
import icon from './images/scan-rec.png'
import Image from 'next/image'
import DOMPurify from 'isomorphic-dompurify'
import HeaderElement from '@/app/ui/header/Header'
import { cookies } from 'next/headers'

async function getData() {
  const res = await fetch(`https://${process.env.API_HOSTNAME}/api/transcripts`)

  if (!res.ok) {
    throw new Error("Failed to fetch transcripts")
  }

  return res.json() as Promise<Array<Transcript>>
}

export default async function Home() {
  const theme = cookies().get('theme')?.value
  const data = await getData();
  data.sort((a, b) => a.episode_number - b.episode_number)
  return (
    <>
      <HeaderElement theme={theme}/>
      <main className="flex min-h-screen flex-col p-8 sm:p-12" data-theme={theme}>
        <div className="flex flex-row">
          <div className="flex flex-col w-2/3">
            <h1
              className="text-4xl font-extrabold leading-none tracking-tight md:text-5xl lg:text-8xl"
            >
              {"Unsettled.FM"}
            </h1>
            <h2
              className="text-3xl font-bold leading-none tracking-tight md:text-4xl lg:text-6xl"
            >
              {"A repository of audio transcriptions and stories"}
            </h2>
            <h3
              className="text-2xl font-semibold leading-none tracking-tight md:text-3xl lg:text-4xl"
            >
              {"Open-Source Whisper-backed speech recognition"}
            </h3>
          </div>
          <div className="w-24 sm:w-36 md:w-48">
            <Image src={icon} sizes="20vw" alt="Icon">
            </Image>
          </div>
        </div>
        <div className="flex items-center gap-2">
          <a className="my-8 btn btn-primary" href="https://wewarnedthem.org/">
            <FaUniversity /> We Warned Them
          </a>
          <a className="my-8 btn btn-neutral" href="https://gitlab.com/cepko33">
            <FaGitlab /> Gitlab
          </a>
        </div>
        <ul className="flex flex-wrap justify-center sm:justify-center md:justify-start mb-4">
          {data.map((transcript: any) => {
            const description = DOMPurify.sanitize(transcript.description, { USE_PROFILES: { html: true } })
            return (
              <li key={`${transcript.title} ${transcript.id}`} className="mb-4">
                <div className="card h-fit sm:h-[40rem] w-72 card-compact mx-4 shadow-lg">
                  <figure className="">
                    <a className="" href={`/transcript/${transcript.id}`}>
                      <img src={transcript.header_photo_url} alt="" className="shadow-inner" />
                    </a>
                  </figure>
                  <div className="card-body shadow-inner">
                    <a className="" href={`/transcript/${transcript.id}`}>
                      <h2 className="text-lg card-title">Episode {transcript.episode_number}</h2>
                      <h2 className="text-xl card-title">{transcript.title}</h2>
                    </a>
                    <div className="overflow-scroll h-60">
                      <h3 className="text-md">{transcript.subtitle}</h3>
                      <div dangerouslySetInnerHTML={{ __html: description }} />
                    </div>
                  </div>
                  <div className="card-actions justify-end m-4">
                    <a className="" href={`/transcript/${transcript.id}`}>
                      <button className="btn btn-neutral">Listen</button>
                    </a>
                  </div>
                </div>
              </li>)
          })
          }
        </ul>
      </main>
    </>
  )
}
