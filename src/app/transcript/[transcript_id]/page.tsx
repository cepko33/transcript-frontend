import React from 'react'
import HeaderElement from '@/app/ui/header/Header'
import TranscriptElement from '@/app/ui/transcript/Transcript'
import Transcript from '@/app/ui/types/Transcript'
import Line from '@/app/ui/types/Line'
import Word from '@/app/ui/types/Word'
import Speaker from '@/app/ui/types/Speaker'
import { cookies } from 'next/headers'
import Head from 'next/head'


async function getData(transcript_id: Number) {
  const res = await fetch(`https://${process.env.API_HOSTNAME}/api/full_transcript/${transcript_id}`)

  if (!res.ok) {
    throw new Error("Failed to fetch transcripts")
  }

  return res.json() as Promise<[Transcript, Array<Speaker>, Array<Line>, Array<Word>]>
}

export default async function Home({ params }: { params: { transcript_id: Number } }) {
  const data = await getData(params.transcript_id);
  const theme = cookies().get('theme')?.value

  return (
    <>
    <HeaderElement theme={theme}/>
    <main className="" data-theme={theme}>
      <TranscriptElement
        inputJSON={data}
      />
    </main>
    </>
  )
}
