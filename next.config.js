/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    API_HOSTNAME: process.env.API_HOSTNAME
  },
  transpilePackages: ['react-daisyui'],
  reactStrictMode: true,
  output: 'standalone',
}

module.exports = nextConfig
